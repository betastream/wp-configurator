<?php

ini_set( 'display_errors', TRUE );
error_reporting( E_ALL | E_STRICT );

return array(
	'constants' => array(
		'DB_CHARSET'  => 'utf8',
		'DB_COLLATE'  => '',
		'DB_HOST'     => 'localhost',
		'DB_NAME'     => '',
		'DB_PASSWORD' => '',
		'DB_USER'     => '',
		
		//'WPLANG'      => '',
		
		'SCRIPT_DEBUG' => TRUE,
		'WP_DEBUG'    => FALSE,
		'WP_DEBUG_DISPLAY' => TRUE,
		
		'AUTOMATIC_UPDATER_DISABLED' => TRUE,
		//'DISALLOW_FILE_EDIT' => TRUE,
		//'DISALLOW_FILE_MODS' => TRUE,
		//'FORCE_SSL_ADMIN' => FALSE,
		//'WP_TEMP_DIR' => __DIR__.'/wp-content/uploads/temp/',
		
		'AUTH_KEY'          => '',
		'SECURE_AUTH_KEY'   => '',
		'LOGGED_IN_KEY'     => '',
		'NONCE_KEY'         => '',
		'AUTH_SALT'         => '',
		'SECURE_AUTH_SALT'  => '',
		'LOGGED_IN_SALT'    => '',
		'NONCE_SALT'        => '',
	),
	
	'globals' => array(
		'table_prefix' => 'wp_'
	)
);